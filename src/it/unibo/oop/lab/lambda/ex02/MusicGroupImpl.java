package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private Optional<String> name = Optional.empty();
    private double countd = 0;
    private int count = 0;
    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public final void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public final void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public final Stream<String> orderedSongNames() {
        Set<String> songstrings = new HashSet<>();
        songs.forEach(s -> {
            songstrings.add(s.songName);
        });
        return songstrings.stream().sorted();
    }

    @Override
    public final Stream<String> albumNames() {
        Set<String> albumstrings = new HashSet<>();
        albums.forEach((k, v) -> {
            albumstrings.add(k);
        });
        return albumstrings.stream();
    }

    @Override
    public final Stream<String> albumInYear(final int year) {
        Set<String> albumstrings = new HashSet<>();
        albums.forEach((k, v) -> {
            if (v == year) {
                albumstrings.add(k);
            }
        });
        return albumstrings.stream();
    }

    @Override
    public final int countSongs(final String albumName) {
        this.count = 0;
        songs.forEach(s -> {
            if (s.albumName.isPresent()) {
                if (albumName.equals(s.albumName.get())) {
                    this.count = this.count + 1;
                }
            }
        });
        return this.count;
    }

    @Override
    public final int countSongsInNoAlbum() {
        this.count = 0;
        songs.forEach(s -> {
            if (!s.albumName.isPresent()) {
                this.count = this.count + 1;
            }
        });
        return this.count;
    }

    @Override
    public final OptionalDouble averageDurationOfSongs(final String albumName) {
        this.countd = 0;
        this.count = 0;
        songs.forEach(s -> {
            if (s.albumName.isPresent()) {
                if (albumName.equals(s.albumName.get())) {
                    this.countd = this.countd + s.duration;
                    this.count++;
                }
            }
        });
        return OptionalDouble.of(this.countd / this.count);
    }

    @Override
    public final Optional<String> longestSong() {
        this.countd = 0;
        songs.forEach(s -> {
            if (this.countd <= s.duration) {
                this.countd = s.duration;
                this.name = Optional.of(s.songName);
            }
        });
        return this.name;
    }

    @Override
    public final Optional<String> longestAlbum() {
        this.countd = 0;
        this.name = Optional.empty();
        Map<String, Double> albumduration = new HashMap<>();
        this.albums.forEach((k, v) -> {
            albumduration.put(k, 0.0);
        });
        songs.forEach(s -> {
            if (s.albumName.isPresent()) {
                albumduration.put(s.albumName.get(), albumduration.get(s.albumName.get()) + s.duration);
            }
        });
        albumduration.forEach((k, v) -> {
            if (this.countd < v) {
                this.countd = v;
                this.name = Optional.of(k);
            }
        });
        return this.name;
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
